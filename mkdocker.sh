#!/bin/sh

set -e

ARCHS=${ARCHS:-amd64:x86_64 arm32v6:arm arm64v8:aarch64 arm32v7:arm}

cat > .gitlab-ci.yml << EOF
include:
  remote: https://gitlab.com/pantacor/ci/ci-templates/raw/master/build-dockerfile.yml

EOF


for a in $ARCHS; do
	arch=`echo $a | sed -e 's/:.*//'`
	cat Dockerfile.template | ARCH=$arch envsubst > Dockerfile.$arch
        cat >> .gitlab-ci.yml << EOF2

build-$arch:
  extends: .build-dockerfile
  variables:
    ARCH: $arch
EOF2

done

