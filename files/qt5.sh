#!/bin/sh

if [ ! -d /run/dbus ]
then
	echo "Shared storage is not mounted"
	exit 1
fi

while [ 1 ]
do
	if [ -e /run/dbus/wayland-0 ]
	then
		break
	fi

	sleep 1
done

sleep 4
mkdir -p /usr/share/X11/xkb

export XDG_RUNTIME_DIR=/run/dbus
export QT_QPA_PLATFORM=wayland
/usr/lib/qt5/bin/qml --platform wayland /logo.qml &

sleep 2
weston-flower &
weston-simple-damage &
weston-presentation-shm &
weston-smoke &
